package id.co.vivahealth.ecomm.autodownloadorder.controller;


import id.co.vivahealth.ecomm.autodownloadorder.domain.EcommBridge;
import id.co.vivahealth.ecomm.autodownloadorder.domain.ListEcommBridgeDAO;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderDTO;
import id.co.vivahealth.ecomm.autodownloadorder.service.impl.OrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/ecomm")
public class OrderController {

    @Autowired
    private OrderServiceImpl orderService;

    @GetMapping("/{storeId}/download")
    public OrderDTO getOrderEcomm(@PathVariable String storeId) {

//        log.info("Hitting EndPoint EcommDownload From Store : " + storeId);

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setHeaders(orderService.getOrderHeaderList(storeId));
        orderDTO.setDetails(orderService.getOrderItemList(storeId));
        orderDTO.setPayments(orderService.getOrderPaymentList(storeId));

        return orderDTO;
    }

    @PostMapping("/uploadbridge")
    public ResponseEntity<ListEcommBridgeDAO> updateBridge(@RequestBody ListEcommBridgeDAO data){

//        log.info("Hitting EndPoint Upload Bridging : " + data);

        List<EcommBridge> ecommBridges = data.getData().parallelStream()
                .map(ecommBridge -> new EcommBridge(ecommBridge.getTrannoExpress(), ecommBridge.getTrannoPos(), ecommBridge.getExternalSourceId()))
                .collect(Collectors.toList());

        for(EcommBridge ecommBridge:ecommBridges){
            log.info("Bridging Data TrannoExpress : " + ecommBridge.getTrannoExpress());
            log.info("Bridging Data TrannoPos : " + ecommBridge.getTrannoPos());
        }

        orderService.updateAll(ecommBridges);
        orderService.updateMIrrorHBridge();

        return  ResponseEntity.ok(data);
    }

}
