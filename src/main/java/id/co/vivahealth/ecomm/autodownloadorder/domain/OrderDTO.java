package id.co.vivahealth.ecomm.autodownloadorder.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    List<OrderHeader> headers;
    List<OrderItem> details;
    List<OrderPayment> payments;
}
