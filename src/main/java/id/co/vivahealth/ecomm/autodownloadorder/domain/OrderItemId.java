package id.co.vivahealth.ecomm.autodownloadorder.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemId implements Serializable {
    private static final long serialVersionUID = 1L;

    private String tranno;
    private String prodId;
}
