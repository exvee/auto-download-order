package id.co.vivahealth.ecomm.autodownloadorder.service;

import id.co.vivahealth.ecomm.autodownloadorder.domain.EcommBridge;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderHeader;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderItem;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderPayment;

import java.util.List;

public interface OrderService {
    List<OrderHeader> getOrderHeaderList(String storeId);
    List<OrderItem> getOrderItemList(String storeId);
    List<OrderPayment> getOrderPaymentList(String storeId);
    boolean updateAll(List<EcommBridge> ecommBridges);
    void updateMIrrorHBridge();
}
