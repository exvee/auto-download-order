package id.co.vivahealth.ecomm.autodownloadorder.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(EcommBridgeId.class)
@Table(name = "ecommbridge")
public class EcommBridge implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @JsonProperty("TrannoExpress")
    private String trannoExpress;

    @Id
    @JsonProperty("Tranno")
    private String trannoPos;

    @JsonProperty("ExternalSourceId")
    private Integer externalSourceId;
}
