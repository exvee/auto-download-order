package id.co.vivahealth.ecomm.autodownloadorder.repositories;

import id.co.vivahealth.ecomm.autodownloadorder.domain.EcommBridge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EcommBridgeRepository extends CrudRepository<EcommBridge, String> {

    @Modifying
    @Query(value = "INSERT INTO [IT_TRANSACTION].[dbo].[h_bridge_tranno] " +
            "(hbtrano_trannoexpress, hbtrano_tranno,[hbtrano_activeyn],[hbtrano_userid],[hbtrano_lastupdate]) " +
            "SELECT [tranno_express], [tranno_pos], 'Y',0,getdate() FROM [ecomm].[dbo].[ecommbridge] " +
            "LEFT JOIN [IT_TRANSACTION].[dbo].[h_bridge_tranno] ON hbtrano_trannoexpress = tranno_express AND hbtrano_tranno = tranno_pos " +
            " WHERE hbtrano_trannoexpress IS NULL AND hbtrano_tranno IS NULL",
            nativeQuery = true)
    void updateMIrrorHBridge();
}
