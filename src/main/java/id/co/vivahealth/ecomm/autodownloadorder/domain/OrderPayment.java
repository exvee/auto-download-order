package id.co.vivahealth.ecomm.autodownloadorder.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "order_payments")
@IdClass(OrderPaymentId.class)
public class OrderPayment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="Vt_Saletrannum")
    private String tranno;

    @Id
    @Column(name="Vt_Paymenttypeid")
    private Integer paymentType;

    @Column(name="Vt_Paymentnumber")
    private String paymentNumber;

    @Column(name="Vt_Corporateid")
    private String corporateId;

    @Column(name="Vt_Amountpayment")
    private BigDecimal amountPayment;

    @Column(name="Vt_Cashbackyn")
    private String cashbackyn;

    @Column(name="Vt_Userid")
    private Integer userid;

    @Column(name="Vt_Lastupdate")
    private Instant lastUpdate;

    @Column(name="Vt_Paymentedcid")
    private Integer paymentEdc;

    @Column(name="Vt_Paymentcardid")
    private Integer paymentCard;

    @Column(name="Vt_Apitransferyn")
    private String apiTransferyn;
}
