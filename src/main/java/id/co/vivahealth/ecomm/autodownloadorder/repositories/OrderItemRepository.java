package id.co.vivahealth.ecomm.autodownloadorder.repositories;

import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderHeader;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository  extends CrudRepository<OrderItem, String> {
    @Query(value = "{call SP_GetEcommOrderItems(:storeId)}", nativeQuery = true)
    public List<OrderItem> findOrderItemWithStoredProcedure(@Param("storeId") String storeId);
}
