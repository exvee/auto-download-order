package id.co.vivahealth.ecomm.autodownloadorder.repositories;


import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderDTO;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderHeader;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderItem;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderHeaderRepository extends CrudRepository<OrderHeader, String> {

    @Query(value = "{call SP_GetEcommOrderHEader(:storeId)}", nativeQuery = true)
    public List<OrderHeader> findOrderHeaderWithStoredProcedure(@Param("storeId") String storeId);


}
