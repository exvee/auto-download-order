package id.co.vivahealth.ecomm.autodownloadorder.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "order_items")
@IdClass(OrderItemId.class)
public class OrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="Vd_Saletrannum")
    private String tranno;

    @Id
    @Column(name="Vd_Prodid")
    private String prodId;

    @Column(name="Vd_Recipenumber")
    private String recipeNumber;

    @Column(name="Vd_Qty")
    private Integer qty;

    @Column(name="Vd_Saleprice")
    private BigDecimal salePrice;

    @Column(name="Vd_Saledisc")
    private BigDecimal saleDisc;

    @Column(name="Vd_Saleamount")
    private BigDecimal saleAmount;

    @Column(name="Vd_Counteryn")
    private String counterYn;

    @Column(name="Vd_Lastupdate")
    private Instant lastUpdate;

    @Column(name="Vd_Userid")
    private Integer userid;

    @Column(name="Vd_Salediscdist")
    private BigDecimal saleDiscDist;

    @Column(name="Vd_Sellpackid")
    private Integer sellpackId;

    @Column(name="Vd_Apitransferyn")
    private String apiTransferyn;

    @Column(name="Vd_Giftpamyn")
    private String giftPamyn;

    @Column(name="Vd_Clinicianid")
    private Integer clinicianId;

    @Column(name="Vd_Saledisccoin")
    private BigDecimal saleDiscCoin;

    @Column(name="Vd_Usedcoin")
    private Integer usedCoin;

    @Column(name="Vd_Extracoin")
    private Integer extraCoin;

    @Column(name="Vd_Clinicianname")
    private String clinicianName;

}
