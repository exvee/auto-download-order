package id.co.vivahealth.ecomm.autodownloadorder.repositories;

import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderItem;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderPayment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderPaymentRepository extends CrudRepository<OrderPayment, String> {

    @Query(value = "{call SP_GetEcommOrderPayments(:storeId)}", nativeQuery = true)
    public List<OrderPayment> findOrderPaymentWithStoredProcedure(@Param("storeId") String storeId);
}
