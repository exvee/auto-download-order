package id.co.vivahealth.ecomm.autodownloadorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class AutoDownloadOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoDownloadOrderApplication.class, args);
	}

}
