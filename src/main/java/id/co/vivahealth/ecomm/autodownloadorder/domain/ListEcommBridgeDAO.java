package id.co.vivahealth.ecomm.autodownloadorder.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListEcommBridgeDAO implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("Data")
    private List<EcommBridge> Data;
}
