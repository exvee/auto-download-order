package id.co.vivahealth.ecomm.autodownloadorder.service.impl;

import id.co.vivahealth.ecomm.autodownloadorder.domain.EcommBridge;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderHeader;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderItem;
import id.co.vivahealth.ecomm.autodownloadorder.domain.OrderPayment;
import id.co.vivahealth.ecomm.autodownloadorder.repositories.EcommBridgeRepository;
import id.co.vivahealth.ecomm.autodownloadorder.repositories.OrderHeaderRepository;
import id.co.vivahealth.ecomm.autodownloadorder.repositories.OrderItemRepository;
import id.co.vivahealth.ecomm.autodownloadorder.repositories.OrderPaymentRepository;
import id.co.vivahealth.ecomm.autodownloadorder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderHeaderRepository orderHeaderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderPaymentRepository orderPaymentRepository;

    @Autowired
    private EcommBridgeRepository ecommBridgeRepository;

    @Override
    public List<OrderHeader> getOrderHeaderList(String storeId) {
        return  orderHeaderRepository.findOrderHeaderWithStoredProcedure(storeId);
    }

    @Override
    public List<OrderItem> getOrderItemList(String storeId) {
        return orderItemRepository.findOrderItemWithStoredProcedure(storeId);
    }

    @Override
    public List<OrderPayment> getOrderPaymentList(String storeId) {
        return orderPaymentRepository.findOrderPaymentWithStoredProcedure(storeId);
    }

    @Override
    public boolean updateAll(List<EcommBridge> ecommBridges) {
        try {
            ecommBridgeRepository.saveAll(ecommBridges);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void updateMIrrorHBridge() {
         ecommBridgeRepository.updateMIrrorHBridge();
    }
}
