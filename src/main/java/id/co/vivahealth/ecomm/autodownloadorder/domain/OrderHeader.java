package id.co.vivahealth.ecomm.autodownloadorder.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "order_headers")
public class OrderHeader implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="Vh_Saletrannum")
    private String tranno;

    @Column(name="Vh_Saletrandate")
    private Instant date;

    @Column(name="Vh_Custid")
    private String custId;

    @Column(name="Vh_Custkinid")
    private String custKin;

    @Column(name="Vh_Regid")
    private String regId;

    @Column(name="Vh_Usedpoint")
    private Integer usedPoint;

    @Column(name="Vh_Totsaleamount")
    private BigDecimal saleAmount;

    @Column(name="Vh_Tottax")
    private BigDecimal tax;

    @Column(name="Vh_Upround")
    private BigDecimal upRound;

    @Column(name="Vh_Downround")
    private BigDecimal downRound;

    @Column(name="Vh_Custpayment")
    private BigDecimal custPayment;

    @Column(name="Vh_Totdiscount")
    private BigDecimal discount;

    @Column(name="Vh_Totvoucher")
    private BigDecimal voucher;

    @Column(name="Vh_Totpayment")
    private BigDecimal totPayment;

    @Column(name="Vh_Employeeid")
    private String employeeId;

    @Column(name="Vh_Userid")
    private Integer userId;

    @Column(name="Vh_Pointid")
    private Integer point;

    @Column(name="Vh_Rewardvalue")
    private BigDecimal reward;

    @Column(name="Vh_Negatifyn")
    private String salesNegatif;

    @Column(name="Vh_Datenegatif")
    private Instant dateNegatif;

    @Column(name="Vh_Reglabid")
    private String regLabId;

    @Column(name="Vh_Conslabyn")
    private String consLab;

    @Column(name="Vh_Dateconslab")
    private Instant dateConsLab;

    @Column(name="Vh_Apitransferyn")
    private String apiTransfer;

    @Column(name="Vh_Coinget")
    private Integer coinGet;

    @Column(name="Vh_Extracoin")
    private Integer coinExtra;

    @Column(name="Vh_external_source_id")
    private Integer externalSourceId;

    @Column(name="Countdetail")
    private Integer countDetail;
}
